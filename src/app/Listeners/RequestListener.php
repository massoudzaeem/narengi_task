<?php

namespace App\Listeners;

use App\Events\RequestEvent;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;
use Sunra\PhpSimple\HtmlDomParser;

class RequestListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */

    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param RequestEvent $event
     * @return void
     */
    public function handle(RequestEvent $event)
    {
        //
        $second = $event;
        // function for crawling
        $url = 'http://64.227.118.51/test/';
        $client = new Client();
        $response = $client->request('GET', $url);
        $html = $response->getBody(true);

        if ($response->getStatusCode() == 200) {
            $dom = HtmlDomParser::str_get_html($html);
            $base_price = $dom->find('span', 0)->text();
//            echo $main_price;
//            while (true) {
//                Redis::set('main_price', $main_price);
//                echo $main_price;
//                sleep($second);
//            }
        }
        Redis::set('base_price', $base_price);
    }
}
