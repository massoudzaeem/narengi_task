<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Hekmatinasser\Verta\Verta;
use Auth;

class Product extends Model
{
    protected $fillable = [
        'user_id', 'title', 'text',
       'quantity','status', 'price'
    ];
    public function product_category()
    {
        return $this->belongsTo(ProductCategory::class, 'category_id');
    }
    public function group_main()
    {
        return $this->belongsTo(Group_main::class);
    }

    public function group_sub1()
    {
        return $this->belongsTo(Group_sub1::class);
    }

    public function group_sub2()
    {
        return $this->belongsTo(Group_sub2::class);
    }

    public function sizes()
    {
        return $this->belongsToMany(Size::class);
    }

    public function colors()
    {
        return $this->belongsToMany(Color::class);
    }

    public function pro_image()
    {
        return $this->belongsToMany(Product_image::class);
    }
    public function order_detail()
    {
        return $this->belongsToMany(Orderdetail::class);
    }
    public function special_product()
    {
        return $this->belongsTo(Special_product::class);
    }

    public function off_product()
    {
        return $this->belongsTo(Off_product::class);
    }

    public function new_product()
    {
        return $this->belongsTo(New_product::class);
    }

    public function favorite_product()
    {
        return $this->belongsTo(Favorite_product::class);
    }

    public function off_pack()
    {
        return $this->belongsTo(Off_pack::class);
    }

    public function special_pack()
    {
        return $this->belongsTo(Special_pack::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
    public function priceCalculation()
    {
        if (Auth::check() && auth()->user()->type == "agent" && $this->agent_price > 0) {
           //$price_model means model gheymat
            $price_model = $this->agent_price ;
        } else {
            $price_model = $this->user_price;
        }
        if ($price_model == 0) {
            return 0;
        } elseif ($this->off_rial || $this->off_percent) {
            if ($this->off_rial) {
                return number_format($price_model - $this->off_rial);
            } elseif ($this->off_percent) {
                return number_format($price_model - ($price_model * $this->off_percent) / 100);
            }
        } else {
            return $price_model;
        }
    }
}
