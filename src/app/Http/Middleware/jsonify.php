<?php

namespace App\Http\Middleware;

use Closure;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\DB;

class jsonify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $client = new Client();
        $user = $request->user();
        $token = DB::table('oauth_access_tokens')->where('user_id', $user->id)->latest()->first()->id;
//        dd($token);
        $request->headers->set('Authorization', $request->headers->get('X-Access-Token'));
        $request->header('Authorization', 'Bearer '.$token);
        return $next($request);

    }
}
