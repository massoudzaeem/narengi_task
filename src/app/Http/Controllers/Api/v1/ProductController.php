<?php

namespace App\Http\Controllers\Api\v1;

use App\Events\RequestEvent;
use App\Product;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminController;
use Illuminate\Support\Facades\Redis;
use Sunra\PhpSimple\HtmlDomParser;

class ProductController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function crawler()
    {
        $url = 'http://64.227.118.51/test/';
        $client=new Client();
        $response = $client->request('GET', $url);
        $html = $response->getBody(true);
        if($response->getStatusCode() == 200){
            $dom = HtmlDomParser::str_get_html($html);
            $base_price = $dom->find('span', 0)->text();
        }
         Redis::set('base_price', $base_price);
    }


    public function index()
    {
        $this->crawler();
//        event(new RequestEvent(10));
        $base_price=Redis::get('base_price');
        $products= Product::select('id','title','text','price','status','quantity')->where('status',1)->get();
        return response(['data'=>['products'=>$products,'base_price'=>$base_price ,'status'=>200], 200]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product=Product::create([
            'user_id'=>1,
            'title'=>'محصول تستی 9',
            'text'=>'توضیحات محصول تستی',
            'price'=>5000,
            'quantity'=>5,
            'status'=>0,
        ]);
        return response()->json([$product,'operationMessage'=>'product added succsfully'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
