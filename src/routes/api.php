<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//    $this->get('/user', function (Request $request) {
//        return $request->user();
//    });
use GuzzleHttp\Client;

Route::group(['prefix' => 'v1', 'namespace' => 'Api\v1'], function () {
    $this->post('signup', 'LoginController@signup')->name('signup');;
    $this->post('login', 'LoginController@login')->name('login');
    $this->get('login', 'LoginController@login')->name('loginPage');
    Route::group(['middleware' => ['jsonify','auth:api', 'BlockAdminUrl']], function () {
        $this->get('logout', 'LoginController@logout');
        $this->resource('products', 'ProductController');
    });

});